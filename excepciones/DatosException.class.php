<?php 

    require '../utils/autoloader.php';

    class DatosException extends Exception{
        private $tabla;
        
        public function __construct($mensaje, $tabla){
            parent::__construct($mensaje);
            $this -> tabla = $tabla;

        }

        public function getTabla(){
            return $this -> tabla;
        }

     }
